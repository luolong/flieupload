Work log
========

10.03.2018
----------

Today, my initial attempt was to set up the project for a continuous integration and continuous delivery in GitLab.
I chose GitLab primarily to test and validate its much vaunted integrated CI/CD pipelines support.
This is all new and exciting for me, so it might take longer than normal to get up and running with a basic project setup, but I guess, the learning experience from this is worth the extra effort.

* Set up Gitlab project
* Setting up GCE account with private Kubernetes cluster
  - Had a few false starts here - yat what happens if you have multiple active Google accounts ...

Time spent ~2h

11.03.2018
----------

Today, my main focus is to get the build and deployment pipeline working.

As it was, I only got half way and had to resume couple of days later...

Time spent ~1h

14.03.2018
----------

My second attempt at creating basic server skeleton project and setting up build pipeline.
The skeleton project was successfully created but setting up CI pipeline using GitLab built-in support for CI/CD takes more time and effort than I am currently willing to invest in this. 

Time spent ~ 4h

15.03.2018
----------

Implement file upload REST endpoint.

Time ~30m

16.03.2018
---------

Implement simplest File upload UI.

Time ~30m

17.03.2018
---------

Now the hard part! As the file is being uploaded, we need to calculate it's hash (SHA1). 
After file has been downloaded, it needs to be moved to where download section can access it 
along with the metadata describing te actual content of the file.

Metadata is a JSON file with roughly the following syntax:
```json
{
  "sha1": "",
  "content-type": "image/jpeg",
  "filename": "some"
}
```   

For this I need to write a custom BodyHandler that will calculate the hash as it is being uploaded. 
After File upload has completed, this hash will be stored in `fileUploads` object of the routing context.

Time ~3h

18.03.2018
---------

So, the original plan did not work out. For some reason, the hash was empty and no matter how I tried, 
the bytes stored on the server would not be added to the digest. Oh well, back to the drawing board.   
  
Because I have no more time to be clever, I will solve this one by brute force—after having uploaded 
the file using default built-in Vert.x body handler, I simply read all the bytes, calculate the hash and
then do all the magic that needs to be done.

Time ~2h

19.03.2018
---------

Download magic.

Most of the time went into understanding what I need from.

Current implementation is rather dumb -- it has no caching, no download of file ranges is 
supported and no HTTP/2 support. 

The download link takes the magic token from download url, checks if the file exists, reads the metadata 
file and sends the file to the response. Simple.

Time ~2h

19.03.2018 (evening)
--------------------

Made Gitlab CI work again.

* Moved the server project to the root to make the build simpler.
* Fixed the `.gitlab-ci.yml` script
* Fixed building of the docker image
* Made the application configurable

Time ~2h

21.03.2018
---------

Made Flashy Drag'n'Drop upload UI. Much proud indeed!

Time ~4h 
