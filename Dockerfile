FROM openjdk:8-jre-alpine

ENV VERTICLE_FILE fileupload-server-fat.jar
ENV VERTICLE_HOME /usr/verticles

ENV UPLOADS_DIR /data/file-uploads
ENV DOWNLOADS_DIR /data/file-downloads

EXPOSE 8080

VOLUME /data

# Copy your fat jar to the container
COPY build/libs/$VERTICLE_FILE $VERTICLE_HOME/
RUN mkdir -p $UPLOADS_DIR && mkdir -p $DOWNLOADS_DIR

# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $VERTICLE_FILE"]
