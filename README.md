File uploader
=============

[![pipeline status](https://gitlab.com/luolong/flieupload/badges/master/pipeline.svg)](https://gitlab.com/luolong/flieupload/commits/master)
[![coverage report](https://gitlab.com/luolong/flieupload/badges/master/coverage.svg)](https://gitlab.com/luolong/flieupload/commits/master)

> As a user I want to upload pictures to a web service.
> 
> Upon uploading I'm returned a 'magical URL' behind which I find the same picture every time. 
> System must recognise if it already has that picture/file and then inform the user, presenting already valid URL to the file.


Building
------

This project uses Gradle Wrapper, so building is really simple:

```bash
$ ./gradlew build shadowJar
```

This will create an eecutable `build/libs/flieupload-server-fat.jar` _Fat Jar_. To run it, use following command line:

```bash
$ java -jar build/libs/fileupload-server-fat.jar
```


Packaging
---------

To build a Docker image of the fat jar, ececute following command line:

```bash
$ docker build -t registry.gitlab.com/luolong/flieupload:latest .
```

This builds docker image capable running the application.

To run the docker image, use following command line:

```bash
$ docker run \
  -v $(pwd)/data:/data \
  -p 8080:8080 \
  registry.gitlab.com/luolong/flieupload:latest
```

Using
----

While the application is running:
 
**Uploading**

1. Point your browser at `http://localhost:8080/`
2. Select file to upload (or drag them to the upload area)
3. (Optionally) press "Upload" button

After upload, the successful response will return a plaintext value with the magic 
URL that will always return the image that was uploaded.

**Downloading**

1. Use the URL provided by the upload response

