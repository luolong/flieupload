package info.tepp.fileupload.server;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.file.FileSystem;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicInteger;

import static info.tepp.fileupload.server.DownloadHandler.DEFAULT_DOWNLOADS_DIRECTORY;

public class UploadHandler implements Handler<RoutingContext> {
    private static final Logger log = LoggerFactory.getLogger(UploadHandler.class);

    private String downloadsDir = DEFAULT_DOWNLOADS_DIRECTORY;

    public UploadHandler setDownloadsDirectory(String downloadsDirectory) {
        this.downloadsDir = downloadsDirectory;
        return this;
    }

    @Override
    public void handle(RoutingContext ctx) {
        FileSystem fs = ctx.vertx().fileSystem();

        ctx.response().setChunked(true);
        ctx.response().write("{\"success\": true,\n");
        ctx.response().write(" \"content\": [");

        AtomicInteger count = new AtomicInteger(ctx.fileUploads().size());
        if (count.get() > 1) ctx.response().write("\n  ");
        ctx.fileUploads().forEach(fu -> {
            log.debug("Uploaded " + fu.fileName() + " as " + fu.uploadedFileName());

            Buffer uploadedFile = fs.readFileBlocking(fu.uploadedFileName());
            String sha1String = calculateSha1(uploadedFile);
            if (log.isTraceEnabled()) log.trace("Uploaded image SHA-1 is " + sha1String);

            String downloadFileName = new File(downloadsDir, sha1String).getPath();
            if (!fs.existsBlocking(downloadFileName)) {
                log.info("New file uploaded with SHA1 " + sha1String);
                FileMetadata fileMetadata = new FileMetadata()
                    .sha1(sha1String)
                    .filename(fu.fileName())
                    .contentType(fu.contentType());

                Buffer metadata = fileMetadata.toBuffer();
                CopyOptions options = new CopyOptions().setAtomicMove(true);
                fs.move(fu.uploadedFileName(), downloadFileName, options, mv -> {
                    fs.writeFileBlocking(downloadFileName + "-metadata.json", metadata);
                    writeResponse(ctx, count, sha1String);
                });
            }
            else {
                log.info("File with SHA1 has already been uploaded: " + sha1String);
                fs.deleteBlocking(fu.uploadedFileName());
                writeResponse(ctx, count, sha1String);
            }
        });
    }

    private void writeResponse(RoutingContext ctx, AtomicInteger count, String sha1String) {
        int i = count.decrementAndGet();
        ctx.response().write("\"");
        ctx.response().write(downloadUrl(ctx, sha1String));
        ctx.response().write("\"");
        if (i > 0) ctx.response().write(",\n  ");
        else ctx.response().end("]}");
    }

    /** Relative download Url */
    private String downloadUrl(RoutingContext ctx, String sha1String) {
        return ctx.request().scheme() + "://" + ctx.request().host() + "/download/" + sha1String;
    }

    private String calculateSha1(Buffer uploadedFile) {
        MessageDigest md = getMessageDigest();
        byte[] digest = md.digest(uploadedFile.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    private MessageDigest getMessageDigest() {
        try {
            return MessageDigest.getInstance("SHA-1");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
