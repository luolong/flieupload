package info.tepp.fileupload.server;

import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

import static info.tepp.fileupload.server.MainVerticle.DEFAULT_DOWNLOADS_DIR;
import static info.tepp.fileupload.server.MainVerticle.DEFAULT_UPLOADS_DIR;

public class MainVerticle extends AbstractVerticle {

    static final String DEFAULT_UPLOADS_DIR = "/data/file-uploads";
    static final String DEFAULT_DOWNLOADS_DIR = "/data/file-downloads";

    @Override
    public void start(Future<Void> startFuture) {
        ConfigRetriever retriever = ConfigRetriever.create(vertx);
        ConfigRetriever.getConfigAsFuture(retriever)
                       .map(Conf::new)
                       .compose(this::start, startFuture);
    }

    private void start(final Conf conf) {
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);

        String uploadsDirectory = conf.getUploadsDirectory();
        ensureDirectoryExists(uploadsDirectory);

        String downloadsDirectory = conf.getDownloadsDirectory();
        ensureDirectoryExists(downloadsDirectory);

        router.post("/upload")
              .handler(BodyHandler.create(uploadsDirectory).setBodyLimit(5_000_000))
              .handler(new UploadHandler().setDownloadsDirectory(downloadsDirectory));

        router.route("/download/:token")
              .method(HttpMethod.HEAD)
              .method(HttpMethod.GET)
              .handler(new DownloadHandler()
                  .setDownloadsDirectory(downloadsDirectory)
                  .setFilenameParameter("token"));

        router.route("/*")
              .handler(StaticHandler.create()
                    .setFilesReadOnly(true)
                    .setDirectoryListing(false)
                    .setIncludeHidden(false)
              );

        final int httpPort = conf.getHttpPort();
        server.requestHandler(router::accept).listen(httpPort);
        System.out.println("HTTP server started on port " + httpPort);
    }

    private void ensureDirectoryExists(final String directory) {
        FileSystem fs = vertx.fileSystem();
        if (!fs.existsBlocking(directory)) {
            fs.mkdirsBlocking(directory);
        }
    }
}

class Conf {
    private final JsonObject json;

    Conf(final JsonObject json) {
        this.json = json.copy();
    }

    public String getUploadsDirectory() {
        return json.getString("UPLOADS_DIR", DEFAULT_UPLOADS_DIR);
    }

    public String getDownloadsDirectory() {
        return json.getString("DOWNLOADS_DIR", DEFAULT_DOWNLOADS_DIR);
    }

    public int getHttpPort() {
        return json.getInteger("http.port", 8080);
    }
}
