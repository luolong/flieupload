package info.tepp.fileupload.server;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;

public class DownloadHandler implements Handler<RoutingContext> {
    private static final Logger log = LoggerFactory.getLogger(DownloadHandler.class);

    static final String DEFAULT_DOWNLOADS_DIRECTORY = StaticHandler.DEFAULT_WEB_ROOT;

    private String downloadsDir = DEFAULT_DOWNLOADS_DIRECTORY;
    private String filenameParam = "token";

    public DownloadHandler setDownloadsDirectory(String downloadsDirectory) {
        this.downloadsDir = downloadsDirectory;
        return this;
    }

    public DownloadHandler setFilenameParameter(String filenameParameterName) {
        this.filenameParam = filenameParameterName;
        return this;
    }

    @Override
    public void handle(RoutingContext context) {
        HttpServerRequest request = context.request();
        if (request.method() != HttpMethod.GET && request.method() != HttpMethod.HEAD) {
            if (log.isTraceEnabled()) log.trace("Not GET or HEAD so ignoring request");
            context.next();
        }
        else {
            String filenameToken = context.request().getParam(this.filenameParam);
            if (filenameToken == null || filenameToken.isEmpty()) {
                log.warn("No filename token specified!");
                context.next();
                return;
            }

            sendStatic(context, filenameToken);
        }

    }

    private void sendStatic(RoutingContext context, String filenameToken) {
        FileSystem fs = context.vertx().fileSystem();

        String filePath = getFilePath(filenameToken);

        fs.exists(filePath, exists -> {
            if (exists.failed()) {
                context.fail(exists.cause());
                return;
            }

            // file does not exist, continue...
            if (!exists.result()) {
                log.warn("File not found: " + filePath);
                context.next();
                return;
            }

            FileMetadata metadata = getFileMetadata(filenameToken, fs);
            sendFile(context, filePath, metadata);
        });
    }

    private String getFilePath(String filenameToken) {
        String file = downloadsDir + "/" + filenameToken;
        if (log.isTraceEnabled()) log.trace("File to serve is " + file);
        return file;
    }

    private FileMetadata getFileMetadata(String filenameToken, FileSystem fs) {
        String metadataFile = downloadsDir + "/" + filenameToken + "-metadata.json";
        if (log.isTraceEnabled()) log.trace("Reading file metadata from " + metadataFile);
        Buffer buffer = fs.readFileBlocking(metadataFile);
        return new FileMetadata(buffer);
    }


    private void sendFile(RoutingContext context, String file, FileMetadata fileMetadata) {
        HttpServerRequest request = context.request();

        if (request.method() == HttpMethod.HEAD) {
            request.response().end();
            return;
        }

        request.response().putHeader(HttpHeaders.CONTENT_TYPE, fileMetadata.getContentType());
        request.response().putHeader("Content-Disposition", "inline; filename=" + fileMetadata.getFilename());

        request.response().sendFile(file, send -> {
            if (send.failed()) {
                context.fail(send.cause());
            }
        });
    }
}
