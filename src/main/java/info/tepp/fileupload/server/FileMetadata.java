package info.tepp.fileupload.server;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;

public class FileMetadata {
    private final JsonObject json;

    public static final class Keys {
        private Keys() {/* sealed */}

        public static final String SHA1 = "sha-1";
        public static final String FILENAME = "filename";
        public static final String CONTENT_TYPE = "content-type";

    }

    public FileMetadata() {
        json = new JsonObject();
    }

    public FileMetadata(Buffer buffer) {
        json = new JsonObject(buffer);
    }

    public String getSha1() {
        return json.getString(Keys.SHA1);
    }

    public String getFilename() {
        return json.getString(Keys.FILENAME);
    }

    public String getContentType() {
        return json.getString(Keys.CONTENT_TYPE);
    }

    public FileMetadata sha1(String sha1) {
        json.put(Keys.SHA1, sha1);
        return this;
    }

    public FileMetadata filename(String filename) {
        json.put(Keys.FILENAME, filename);
        return this;
    }

    public FileMetadata contentType(String contentType) {
        json.put(Keys.CONTENT_TYPE, contentType);
        return this;
    }

    public Buffer toBuffer() {
        return json.toBuffer();
    }
}
